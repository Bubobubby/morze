def code_morze(value):
    '''
    please add your solution here or call your solution implemented in different function from here  
    then change return value from 'False' to value that will be returned by your solution
    '''
    supplementary = {'A': '.-', 'B': '-...', 'C': '-.-.', 'D': '-..', 'E': '.', 'F': '..-.', 'G': '--.', 'H': '....',
                       'I': '..', 'J': '.---', 'K': '-.-', 'L': '.-..', 'M': '--', 'N': '-.', 'O': '---', 'P': '.--.',
                       'Q': '--.-', 'R': '.-.', 'S': '...', 'T': '-', 'U': '..-', 'V': '...-', 'W': '.--', 'X': '-..-',
                       'Y': '-.--', 'Z': '--..', '1': '.----', '2': '..---', '3': '...--', '4': '....-', '5': '.....',
                       '6': '-....', '7': '--...', '8': '---..', '9': '----.', '0': '-----', ', ': '--..--', '.': '.-.-.-',
                       '?': '..--..', '/': '-..-.', '-': '-....-', '(': '-.--.', ')': '-.--.-'
                       }

    text = value.upper()
    morse_message = []

    for char in text:
        if char in supplementary:
            morse_letter = supplementary[char]
            morse_message.append(morse_letter)

    morse_message_str = ' '.join(morse_message)
    return morse_message_str

text_message = str(input('Write sentence: '))
# text_message = "Data Science - 2023"
morse_code = code_morze(text_message)
print(morse_code)

if __name__ == "__main__":
    assert code_morze("Data Science - 2023") == ('-.. .- - .- ... -.-. .. . -. -.-. . -....- ..--- ----- ..--- ...--')